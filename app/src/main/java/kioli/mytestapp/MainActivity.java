package kioli.mytestapp;

import android.app.Activity;
import android.os.Bundle;


public class MainActivity extends Activity {

    public final static String FRAGMENT_TAG = "frg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setFragment();
    }

    private void setFragment(){
        getFragmentManager().beginTransaction().add(R.id.frame, new MainFragment(), FRAGMENT_TAG).commit();
    }

}
