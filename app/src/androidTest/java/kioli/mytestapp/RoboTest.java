package kioli.mytestapp;

import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(TestRunner.class)
public class RoboTest {

    @Test
    public void checkAppExist() {
        assertThat(Robolectric.application, notNullValue());
    }

    @Test
    public void checkActivityExist() throws Exception {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        assertThat(activity, notNullValue());
    }

    @Test
    public void checkFragmentExist() throws Exception {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        MainFragment fragment = (MainFragment) activity.getFragmentManager().findFragmentByTag(activity.FRAGMENT_TAG);
        assertThat(fragment, notNullValue());
    }

    @Test
    public void checkTextIsOk() throws Exception {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        MainFragment fragment = (MainFragment) activity.getFragmentManager().findFragmentByTag(activity.FRAGMENT_TAG);
        TextView results = (TextView) fragment.getView().findViewById(R.id.text_fragment);
        assertEquals(results.getText().toString(), "Hello fragment!");
    }
}
